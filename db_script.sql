
use eu_business;


drop table if exists distributor; 
drop table if exists manufacturer; 
drop table if exists inventory_item; 
drop table if exists inventory; 
drop table if exists order_items; 
drop table if exists orders;

drop table if exists customer; 




create table customer (
	customer_id int not null primary key auto_increment,
    first_name varchar(50), 
    last_name varchar(50), 
    phone_number varchar(50), 
    address varchar(200)
); 

create table distributor (
	distributor_id int not null primary key auto_increment,
    name varchar(50) not null
); 

create table manufacturer (
	manufacturer_id int not null primary key auto_increment,
    name varchar(50)
); 

create table inventory_item (
	inventory_item_id int not null primary key auto_increment,
    manufacturer_part_num varchar(50) not null, 
    manufacturer_id int not null, 
    distributor_id int not null, 
    description varchar(100) not null, 
    dist_cost double not null, 
    sell_price double not null, 
    min_qty int,
    max_qty int,
  foreign key (distributor_id) references distributor(distributor_id) on delete no action,
    foreign key (manufacturer_id) references manufacturer(manufacturer_id) on delete no action
);

create table inventory (
	inventory_item_id int not null primary key,
  qty int,
    foreign key (inventory_item_id) references inventory_item(inventory_item_id) on delete no action
); 
   
create table orders (
	order_id int not null primary key auto_increment,
	customer_id int,
	date_opened timestamp,
	last_modified_date timestamp, 
	down_payment double not null, 
	total_price double,
	status char(1), -- 'E' estimate 'W' Work order 'I' Invoice
	notes varchar(200), 
	foreign key (customer_id) references customer(customer_id) on delete no action
);

create table order_items(
	order_id int primary key,
	inventory_item_id int primary key,
	qty int,
	foreign key (order_id) references orders(order_id) on delete no action,
	foreign key (inventory_item_id) references inventory_item(inventory_item_id) on delete no action
);

    

 	