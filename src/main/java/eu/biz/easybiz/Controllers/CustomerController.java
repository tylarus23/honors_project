package eu.biz.easybiz.Controllers;

import eu.biz.easybiz.Entities.Customer;
import eu.biz.easybiz.Entities.Inventory;
import eu.biz.easybiz.Repositories.CustomerRepository;
import eu.biz.easybiz.Repositories.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/customers")
public class CustomerController
{
    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping("/all")
    public Iterable<Customer> viewAllCustomers()
    {
        return customerRepository.findAll();
    }

    @PostMapping("/add")
    public ResponseEntity<HttpStatus> addNewCustomer(@RequestBody Customer customer)
    {
        customerRepository.save(customer);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value="/delete", method=RequestMethod.POST)
    public ResponseEntity<HttpStatus> deleteCustomer(@RequestBody Customer customer) {
        customerRepository.delete(customer);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
