package eu.biz.easybiz.Controllers;

import eu.biz.easybiz.Entities.Distributor;
import eu.biz.easybiz.Entities.Manufacturer;
import eu.biz.easybiz.Repositories.DistributorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/distributor")
public class DistributorController
{
    @Autowired
    private DistributorRepository distributorRepository;

    @GetMapping("/all")
    public Iterable<Distributor> viewAllCustomers()
    {
        return distributorRepository.findAll();
    }

    @PostMapping("add")
    public ResponseEntity<HttpStatus> addNewDistributor(@RequestBody Distributor distributor)
    {
        distributorRepository.save(distributor);

        return new ResponseEntity<>(HttpStatus.OK);
    }
    @RequestMapping(value="/delete", method=RequestMethod.POST)
    public ResponseEntity<HttpStatus> deleteDistributor(@RequestBody Distributor distributor) {
        distributorRepository.delete(distributor);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
