package eu.biz.easybiz.Controllers;

import eu.biz.easybiz.Entities.*;
import eu.biz.easybiz.Repositories.DistributorRepository;
import eu.biz.easybiz.Repositories.InventoryItemRepository;
import eu.biz.easybiz.Repositories.InventoryRepository;
import eu.biz.easybiz.Repositories.ManufacturerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController
@RequestMapping("/inventory")
public class InventoryController
{

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private InventoryItemRepository itemRepository;

    @Autowired
    private InventoryRepository iRepository;

    @Autowired
    private ManufacturerRepository mRepository;

    @Autowired
    private DistributorRepository dRepository;


    @GetMapping("/all")
    public Iterable<InventoryData> viewInventoryItems()
    {
        ArrayList<InventoryData> data = new ArrayList<>();
        Iterable<InventoryItem> temp = itemRepository.findAll();
        for (InventoryItem item : temp)
        {
            InventoryData itemData = new InventoryData();


            itemData.setInventory_item_id(item.getInventory_item_id());
            itemData.setManufacturer_id(item.getManufacturer_id());
            itemData.setManufacturer_part_num(item.getManufacturer_part_num());
            itemData.setDistributor_id(item.getDistributor_id());
            itemData.setDescription(item.getDescription());
            itemData.setDist_cost(item.getDist_cost());
            itemData.setSell_price(item.getSell_price());
            itemData.setMin_qty(item.getMin_qty());
            itemData.setMax_qty(item.getMax_qty());

            itemData.setQty(
                    inventoryRepository.findOne(item.getInventory_item_id()).getQty()
            );
            itemData.setManufacturer_name(
                    mRepository.findOne(item.getManufacturer_id()).getName()
            );

            itemData.setDistributor_name(
                    dRepository.findOne(item.getDistributor_id()).getName()
            );

            data.add(itemData);
        }


        return data;
    }

    @PostMapping("/add")
    public ResponseEntity<HttpStatus> addNewItem(@RequestBody InventoryData data)
    {
        InventoryItem item = new InventoryItem();
        item.setDescription(data.getDescription());
        item.setManufacturer_id(data.getManufacturer_id());
        item.setManufacturer_part_num(data.getManufacturer_part_num());
        item.setDistributor_id(data.getDistributor_id());
        item.setDist_cost(data.getDist_cost());
        item.setSell_price(data.getSell_price());
        item.setMin_qty(data.getMin_qty());
        item.setMax_qty(data.getMax_qty());

        item = itemRepository.save(item);

        Inventory inventory = new Inventory();
        inventory.setInventory_item_id(item.getInventory_item_id());
        inventory.setQty(data.getQty());

        iRepository.save(inventory);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/getManufacturers")
    public Iterable<Manufacturer> getManufacturers() {
        return mRepository.findAll();
    }

    @PostMapping("/getDistributors")
    public Iterable<Distributor> getDistributors() {
        return dRepository.findAll();
    }


    @PostMapping("/updateQty")
    public ResponseEntity<HttpStatus> updateQty(@RequestBody InventoryData inventoryData) {
        Inventory inventory = new Inventory();
        inventory.setInventory_item_id(inventoryData.getInventory_item_id());
        inventory.setQty(inventoryData.getQty());

        inventoryRepository.save(inventory);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
