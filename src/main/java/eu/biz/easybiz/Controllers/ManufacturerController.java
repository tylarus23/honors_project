package eu.biz.easybiz.Controllers;

import eu.biz.easybiz.Entities.Manufacturer;
import eu.biz.easybiz.Repositories.ManufacturerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/manufacturer")
public class ManufacturerController
{
    @Autowired
    private ManufacturerRepository manufacturerRepository;

    @GetMapping("/all")
    public Iterable<Manufacturer> viewAllManufacturers()
    {
        return manufacturerRepository.findAll();
    }

    @PostMapping("add")
    public ResponseEntity<HttpStatus> addNewManufacturer(@RequestBody Manufacturer manufacturer)
    {
        manufacturerRepository.save(manufacturer);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value="/delete", method=RequestMethod.POST)
    public ResponseEntity<HttpStatus> deleteManufacturer(@RequestBody Manufacturer m) {
        manufacturerRepository.delete(m);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
