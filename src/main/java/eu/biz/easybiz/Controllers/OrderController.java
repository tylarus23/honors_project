package eu.biz.easybiz.Controllers;

import eu.biz.easybiz.Entities.*;
import eu.biz.easybiz.Repositories.CustomerRepository;
import eu.biz.easybiz.Repositories.InventoryItemRepository;
import eu.biz.easybiz.Repositories.OrderItemsRepository;
import eu.biz.easybiz.Repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;

@RestController
@RequestMapping("/orders")
public class OrderController {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderItemsRepository orderItemRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private InventoryItemRepository inventoryItemRepository;

    @GetMapping("/all")
    public Iterable<OrderData> getAllOrders() {
        ArrayList<OrderData> data = new ArrayList<>();

        Iterable<Orders> temp = orderRepository.findAll();

        for(Orders order : temp) {
            OrderData inputData = new OrderData();

            inputData.setOrder_id(order.getOrder_id());
            inputData.setDescription(order.getDescription());
            inputData.setCustomer_id(order.getCustomer_id());
            inputData.setCustomer_name(
                    customerRepository.findOne(order.getCustomer_id()).getFirst_name() + " "
                    + customerRepository.findOne(order.getCustomer_id()).getLast_name()
            );

            inputData.setDate_opened(order.getDate_opened());
            inputData.setLast_modified_date(order.getLast_modified_date());
            inputData.setDown_payment(order.getDown_payment());
            inputData.setTotal_price(order.getTotal_price());
            inputData.setStatus(order.getStatus());
            inputData.setNotes(order.getNotes());


            Iterable<OrderItems> orderItems = orderItemRepository.findByOrder(order.getOrder_id());
            ArrayList<InventoryData> items = new ArrayList<>();

            for (OrderItems i : orderItems) {
                InventoryData inventoryData = new InventoryData();
                InventoryItem inventoryItem = inventoryItemRepository.findOne(i.getInventory_item_id());

                inventoryData.setInventory_item_id(i.getInventory_item_id());
                inventoryData.setDescription(inventoryItem.getDescription());
                inventoryData.setQty(i.getQty());

                inputData.setTotal_price(inputData.getTotal_price() + (inventoryItem.getSell_price() * i.getQty()));
                items.add(inventoryData);
            }

            inputData.setItems(items);
            data.add(inputData);
        }

        return data;
    }

    @PostMapping("/getItems")
    public Iterable<InventoryData> getItems(@RequestBody Orders order) {

        ArrayList<InventoryData> orderItems = new ArrayList<>();
        Iterable<OrderItems> items = orderItemRepository.findByOrder(order.getOrder_id());

        for (OrderItems item : items) {
            InventoryData temp = new InventoryData();

            temp.setInventory_item_id(item.getInventory_item_id());
            temp.setQty(item.getQty());

            InventoryItem tempItem = inventoryItemRepository.findOne(item.getInventory_item_id());
            temp.setDescription(tempItem.getDescription());

            orderItems.add(temp);

        }

        return orderItems;
    }

    @PostMapping("/add")
    public ResponseEntity<HttpStatus> getItems(@RequestBody OrderData data)
    {
        Orders order = new Orders();

        order.setOrder_id(data.getOrder_id());
        order.setCustomer_id(data.getCustomer_id());
        order.setDate_opened(new Date(System.currentTimeMillis()));
        order.setDescription(data.getDescription());
        order.setNotes("");
        order.setDown_payment(0.00);
        order.setStatus('E');
        order.setTotal_price(0.00);

        orderRepository.save(order);

        return new ResponseEntity<HttpStatus>(HttpStatus.OK);
    }

    @PostMapping("/addOrderItems")
    public ResponseEntity<HttpStatus> addOrderItems(@RequestBody Iterable<InventoryData> data) {

        for (InventoryData item : data) {
            if (orderItemRepository.findOne(new OrderItemsPK(item.getAssigned_order(), item.getInventory_item_id())) != null) {
                OrderItems orderItemsToUpdate = orderItemRepository.findOne(new OrderItemsPK(item.getAssigned_order(), item.getInventory_item_id()));
                orderItemsToUpdate.setQty(orderItemsToUpdate.getQty() + item.getQty());
                orderItemRepository.save(orderItemsToUpdate);
            }
            else {
                OrderItems newOrderItem = new OrderItems();
                newOrderItem.setOrder_id(item.getAssigned_order());
                newOrderItem.setInventory_item_id(item.getInventory_item_id());
                newOrderItem.setQty(item.getQty());

                orderItemRepository.save(newOrderItem);
            }
        }

        return new ResponseEntity<HttpStatus>(HttpStatus.OK);
    }

    @PostMapping("/generateWorkOrder")
    public ResponseEntity<HttpStatus> generateWorkOrder(@RequestBody OrderData data) {
        Orders order = new Orders();

        order.setOrder_id(data.getOrder_id());
        order.setCustomer_id(data.getCustomer_id());
        order.setDown_payment(data.getDown_payment());
        order.setDate_opened(data.getDate_opened());
        order.setLast_modified_date(data.getLast_modified_date());
        order.setDescription(data.getDescription());
        order.setNotes(data.getNotes());
        order.setStatus('W');
        order.setTotal_price(order.getTotal_price());

        orderRepository.save(order);

        return new ResponseEntity<HttpStatus>(HttpStatus.OK);
    }

    @PostMapping("/generateInvoice")
    public ResponseEntity<HttpStatus> generateInvoice(@RequestBody OrderData data) {
        Orders order = new Orders();

        order.setOrder_id(data.getOrder_id());
        order.setCustomer_id(data.getCustomer_id());
        order.setDown_payment(data.getDown_payment());
        order.setDate_opened(data.getDate_opened());
        order.setLast_modified_date(data.getLast_modified_date());
        order.setDescription(data.getDescription());
        order.setNotes(data.getNotes());
        order.setStatus('I');
        order.setTotal_price(order.getTotal_price());

        orderRepository.save(order);

        return new ResponseEntity<HttpStatus>(HttpStatus.OK);
    }
}
