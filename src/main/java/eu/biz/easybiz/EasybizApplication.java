package eu.biz.easybiz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasybizApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasybizApplication.class, args);
	}
}
