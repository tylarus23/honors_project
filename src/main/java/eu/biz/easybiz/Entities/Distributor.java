package eu.biz.easybiz.Entities;

import javax.persistence.*;

@Entity
public class Distributor {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int distributor_id;

    private String name;

    public int getDistributor_id() {
        return distributor_id;
    }

    public void setDistributor_id(int distributor_id) {
        this.distributor_id = distributor_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}