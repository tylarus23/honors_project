package eu.biz.easybiz.Entities;

import javax.persistence.*;

// https://spring.io/guides/gs/accessing-data-mysql/

@Entity // Creates a table
public class Inventory {
    @Id
    private int inventory_item_id;

    private int qty;

    public int getInventory_item_id() {
        return inventory_item_id;
    }

    public void setInventory_item_id(int inventory_item_id) {
        this.inventory_item_id = inventory_item_id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
