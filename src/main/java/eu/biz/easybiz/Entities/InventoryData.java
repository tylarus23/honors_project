package eu.biz.easybiz.Entities;

import javax.persistence.CascadeType;
import javax.persistence.OneToOne;

public class InventoryData {
    private int inventory_item_id;

    private int manufacturer_id;
    private String manufacturer_name;
    private String manufacturer_part_num;

    private int distributor_id;
    private String distributor_name;

    private String description;
    private double dist_cost;
    private double sell_price;
    private int min_qty;
    private int max_qty;

    private int qty;

    private int assigned_order;

    public int getInventory_item_id() {
        return inventory_item_id;
    }

    public void setInventory_item_id(int inventory_item_id) {
        this.inventory_item_id = inventory_item_id;
    }

    public int getManufacturer_id() {
        return manufacturer_id;
    }

    public void setManufacturer_id(int manufacturer_id) {
        this.manufacturer_id = manufacturer_id;
    }

    public String getManufacturer_part_num() {
        return manufacturer_part_num;
    }

    public void setManufacturer_part_num(String manufacturer_part_num) {
        this.manufacturer_part_num = manufacturer_part_num;
    }

    public int getDistributor_id() {
        return distributor_id;
    }

    public void setDistributor_id(int distributor_id) {
        this.distributor_id = distributor_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getDist_cost() {
        return dist_cost;
    }

    public void setDist_cost(double dist_cost) {
        this.dist_cost = dist_cost;
    }

    public double getSell_price() {
        return sell_price;
    }

    public void setSell_price(double sell_price) {
        this.sell_price = sell_price;
    }

    public int getMin_qty() {
        return min_qty;
    }

    public void setMin_qty(int min_qty) {
        this.min_qty = min_qty;
    }

    public int getMax_qty() {
        return max_qty;
    }

    public void setMax_qty(int max_qty) {
        this.max_qty = max_qty;
    }

    public String getManufacturer_name() {
        return manufacturer_name;
    }

    public void setManufacturer_name(String manufacturer_name) {
        this.manufacturer_name = manufacturer_name;
    }

    public String getDistributor_name() {
        return distributor_name;
    }

    public void setDistributor_name(String distributor_name) {
        this.distributor_name = distributor_name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getAssigned_order() {
        return assigned_order;
    }

    public void setAssigned_order(int assigned_order) {
        this.assigned_order = assigned_order;
    }
}
