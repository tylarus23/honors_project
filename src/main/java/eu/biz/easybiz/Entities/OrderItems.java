package eu.biz.easybiz.Entities;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(OrderItemsPK.class)
public class OrderItems {
    @Id
    private int order_id;
    @Id
    private int inventory_item_id;
    private int qty;

    public OrderItems() {

    }
    public int getOrder_id() {
        return order_id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getInventory_item_id() {
        return inventory_item_id;
    }

    public void setInventory_item_id(int inventory_item_id) {
        this.inventory_item_id = inventory_item_id;
    }
}