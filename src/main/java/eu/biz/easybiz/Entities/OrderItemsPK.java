package eu.biz.easybiz.Entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;


public class OrderItemsPK implements Serializable {
    private int order_id;
    private int inventory_item_id;

    public OrderItemsPK () {

    }
    public OrderItemsPK(int order_id, int inventory_item_id) {
        this.order_id = order_id;
        this.inventory_item_id = inventory_item_id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getInventory_item_id() {
        return inventory_item_id;
    }

    public void setInventory_item_id(int inventory_item_id) {
        this.inventory_item_id = inventory_item_id;
    }
}
