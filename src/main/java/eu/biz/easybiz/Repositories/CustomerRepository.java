package eu.biz.easybiz.Repositories;

import eu.biz.easybiz.Entities.Customer;
import eu.biz.easybiz.Entities.Inventory;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Integer>
{

}
