package eu.biz.easybiz.Repositories;

import eu.biz.easybiz.Entities.Distributor;
import org.springframework.data.repository.CrudRepository;

public interface DistributorRepository extends CrudRepository<Distributor, Integer>
{

}