package eu.biz.easybiz.Repositories;

import eu.biz.easybiz.Entities.InventoryItem;
import org.springframework.data.repository.CrudRepository;

public interface InventoryItemRepository extends CrudRepository<InventoryItem, Integer>
{

}