package eu.biz.easybiz.Repositories;

import eu.biz.easybiz.BizResources.SQLConstants;
import eu.biz.easybiz.Entities.Inventory;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface InventoryRepository extends CrudRepository<Inventory, Integer>
{

}
