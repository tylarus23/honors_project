package eu.biz.easybiz.Repositories;

import eu.biz.easybiz.Entities.Manufacturer;
import org.springframework.data.repository.CrudRepository;

public interface ManufacturerRepository extends CrudRepository<Manufacturer, Integer> {

}