package eu.biz.easybiz.Repositories;

import eu.biz.easybiz.Entities.OrderItems;
import eu.biz.easybiz.Entities.OrderItemsPK;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface OrderItemsRepository extends CrudRepository<OrderItems, OrderItemsPK>
{
    @Query("SELECT o FROM OrderItems o WHERE o.order_id = :order_id")
    Iterable<OrderItems> findByOrder(@Param("order_id") Integer order_id);


}
