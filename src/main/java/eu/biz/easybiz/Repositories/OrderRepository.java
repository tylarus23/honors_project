package eu.biz.easybiz.Repositories;

import eu.biz.easybiz.Entities.Orders;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Orders, Integer>
{

}