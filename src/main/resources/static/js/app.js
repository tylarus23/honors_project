
var app = angular.module('app', ['ngRoute', 'ngResource', 'ngMaterial']);
app.config(function($routeProvider) {
    $routeProvider
        .when('/customers', {
            templateUrl: '/views/customers.html',
            controller: 'customerController'
        })
        .when('/manufacturers', {
            templateUrl: '/views/manufacturers.html',
            controller: 'manufacturerController'
        })
        .when('/distributors', {
            templateUrl: '/views/distributors.html',
            controller: 'distributorController'
        })
        .when('/inventory', {
            templateUrl: '/views/inventory.html',
            controller: 'inventoryController'
        })
        .when('/orders', {
            templateUrl: '/views/orders.html',
            controller: 'orderController'
        })
        .when('/viewEstimate', {
            templateUrl: '/views/view-estimate.html',
            controller: 'orderController'
        })
        .when('/viewWorkOrder', {
            templateUrl: '/views/view-work-order.html',
            controller: 'orderController'
        })
        .when('/viewInvoice', {
            templateUrl: '/views/view-invoice.html',
            controller: 'orderController'
        })
        .otherwise(
            {
                redirectTo:'/'
            }
        )
});

