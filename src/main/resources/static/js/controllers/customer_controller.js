app.controller("customerController", ['$scope', 'customerService', '$mdDialog', function($scope, customerService, $mdDialog) {
    $scope.headingTitle = "Customers";

    var self = this;

    self.customer = {
        customer_id: null,
        first_name: '',
        last_name:  '',
        phone_number: '',
        address: ''};

    self.customers=[];

    self.submit = submit;
    self.remove = remove;
    /*
    self.edit = edit;
    self.reset = reset; */

    viewAllCustomers();

    function viewAllCustomers() {
        customerService.viewAllCustomers()
            .then (
                function(data) {
                    self.customers = data;
                },
                function(errResponse) {
                    console.error('Error while fetching Customers');
                }
            );
    }

    function addCustomer(customer) {
        customerService.addCustomer(customer)
            .then(
                viewAllCustomers,
                function(errResponse) {
                    console.error('Error while adding Customer');
                }
            );
    }

    function deleteCustomer(customer) {
        customerService.deleteCustomer(customer)
            .then(
                viewAllCustomers,
                function(errResponse) {
                    console.error('Error while adding Customer');
                    alert("Could not delete customer because it is currently being used in a order in progress.");
                }
            )
    }

    function submit() {
        console.log('Saving new Customer', self.customer);
        addCustomer(self.customer);


        reset();
    }

    function remove(customer) {
        console.log('Deleting Customer', customer);
        deleteCustomer(customer);
    }

    function reset(){
        self.customer={id:null,first_name:'',last_name:'', phone_number:'', address:''};
    }

    // Dialog logic

    $scope.showAddCustomer = function(ev) {
        $mdDialog.show({
            controller: AddCustomerCntl,
            templateUrl: '../views/dialogs/add-customer.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: $scope.customFullScreen
        }).then(function(answer) {

        }, function() {

        });
    };


    function AddCustomerCntl($scope, $mdDialog) {
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.send = function(fn, ln, ph, add) {
            self.customer.address = add;
            self.customer.first_name = fn;
            self.customer.last_name = ln;
            self.customer.phone_number = ph;
            submit();
            $mdDialog.hide();
        };
    }
}]);



