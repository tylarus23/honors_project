app.controller("distributorController", ['$scope', 'distributorService', '$mdDialog', function($scope, distributorService, $mdDialog) {
    $scope.headingTitle = "Distributors";

    var self = this;

    self.distributor = {
        distributor_id: null,
        name: ' '
    };

    self.distributors=[];

    self.submit = submit;
    self.remove = remove;
    /*
    self.edit = edit;
    self.reset = reset; */

    viewAllDistributors();

    function viewAllDistributors() {
        distributorService.viewAllDistributors()
            .then (
                function(data) {
                    self.distributors = data;
                },
                function(errResponse) {
                    console.error('Error while fetching Distributors');
                }
            );
    }

    function addDistributor(distributor) {
        distributorService.addDistributor(distributor)
            .then(
                viewAllDistributors,
                function(errResponse) {
                    console.error('Error while adding Distributors');
                }
            );
    }

    function deleteDistributor(distributor) {
        distributorService.deleteDistributor(distributor)
            .then(
                viewAllDistributors,
                function(errResponse) {
                    console.error('Error while adding Customer');
                    alert("Could not delete distributor because it is currently associated with items in inventory");
                }
            )
    }

    function submit() {
        console.log('Saving new Customer', self.distributor);
        addDistributor(self.distributor);


        reset();
    }

    function remove(distributor) {
        console.log('Deleting Customer', distributor);
        deleteDistributor(distributor);
    }

    function reset(){
        self.distributor={id:null,name:''};
    }

    // Dialog logic

    $scope.showAddDistributor = function(ev) {
        $mdDialog.show({
            controller: AddDistributorCntl,
            templateUrl: '../views/dialogs/add-distributor.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: $scope.customFullScreen
        }).then(function(answer) {

        }, function() {

        });
    };


    function AddDistributorCntl($scope, $mdDialog) {
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.send = function(nm) {
            self.distributor.name = nm;
            submit();
            $mdDialog.hide();
        };
    }
}]);
