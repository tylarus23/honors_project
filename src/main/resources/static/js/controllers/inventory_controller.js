app.controller("inventoryController", ['$scope', 'inventoryService', '$mdDialog', function($scope, inventoryService, $mdDialog) {
    $scope.headingTitle = "Inventory";

    var self = this;

    self.item = {
        inventory_item_id: null,
        manufacturer_id: '',
        manufacturer_name: '',
        manufacturer_part_num: '',
        distributor_id: '',
        distributor_name: '',
        description: '',
        dist_cost: '',
        sell_price: '',
        min_qty: '',
        max_qty: '',
        qty: '',
        assigned_order: null
    };

    self.items=[];
    self.manufacturers=[];
    self.distributors=[];

    self.manufacturer = {
        manufacturer_id: '',
        name: ''
    };

    self.distributor = {
        distributor_id: '',
        name: ''
    };

    self.updateId = {
        id: ''
    };

    self.submitNewItem = submitNewItem;
    //self.remove = remove;

   // self.edit = edit;
    self.reset = reset;

    viewAllInventory();
    getManufacturers();
    getDistributors();

    function viewAllInventory() {
        inventoryService.viewAllInventory()
            .then (
                function(data) {
                    self.items = data;
                },
                function(errResponse) {
                    console.error('Error while fetching Inventory');
                }
            );
    }

    function getManufacturers() {
        inventoryService.getManufacturers()
            .then(
                function (data) {
                    self.manufacturers = data;
                },

                function(errResponse) {
                    console.error("Error while fetching Manufacturers");
                }
            );
    }

    function getDistributors() {
        inventoryService.getDistributors()
            .then(
                function (data) {
                    self.distributors = data;
                },

                function(errResponse) {
                    console.error("Error while fetching Distributors");
                }
            );
    }


    function addItem(item) {
        inventoryService.addItem(item)
            .then(
                viewAllInventory,
                function(errResponse) {
                    console.error('Error while adding Inventory Item');
                }
            );
    }

    function updateQty(item) {
        inventoryService.updateQty(item)
            .then(
            viewAllInventory,
            function(errResponse) {
                console.error('Error while adding Inventory Item');
            }
        );
    }
    /*
    function deleteCustomer(customer) {
        customerService.deleteCustomer(customer)
            .then(
                viewAllCustomers,
                function(errResponse) {
                    console.error('Error while adding Customer');
                }
            )
    } */


    function submitNewItem() {
        console.log('Saving new Inventory item', self.item);
        addItem(self.item);


        reset();
    }

    function submitNewQty() {
        console.log("Saving new Qty", self.item);
        updateQty(self.item);

        reset();
    }
    /*
    function remove(customer) {
        console.log('Deleting Customer', customer);
        deleteCustomer(customer);
    } */

    function reset(){
        self.item = {
            inventory_item_id: null,
            manufacturer_id: '',
            manufacturer_name: '',
            manufacturer_part_num: '',
            distributor_id: '',
            distributor_name: '',
            description: '',
            dist_cost: '',
            sell_price: '',
            min_qty: '',
            max_qty: '',
            qty: '',
            assigned_order: null
        };
    }

    $scope.more = false;

    $scope.toggleData = function (ev) {
        console.log("hello");
        if ($scope.more === true) {
            $scope.more = false;
        }
        else if ($scope.more === false) {
            $scope.more = true;
        }
    };

    // Dialog logic

    $scope.showAddInventory = function(ev) {
        $mdDialog.show({
            controller: AddInventoryCntl,
            templateUrl: '../views/dialogs/add-inventory.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: $scope.customFullScreen
        }).then(function(answer) {

        }, function() {

        });
    };

    $scope.showUpdateQty = function(ev, id) {
        setUpdateId(id);
        $mdDialog.show({
            controller: UpdateQtyCntl,
            templateUrl: '../views/dialogs/update-qty.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: $scope.customFullScreen
        }).then(function (answer) {

        }, function() {

        });
    };

    function setUpdateId(id) {
        self.updateId.id = id;
    }

    function AddInventoryCntl($scope, $mdDialog) {

        $scope.manufacturers = self.manufacturers;
        $scope.distributors = self.distributors;

        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.send = function(mId, mPart, dId, d, dCost, sell, min, max, qty) {
            self.item.manufacturer_id = mId;
            self.item.manufacturer_part_num = mPart;
            self.item.distributor_id = dId;
            self.item.description = d;
            self.item.dist_cost = dCost;
            self.item.sell_price = sell;
            self.item.min_qty = min;
            self.item.max_qty = max;
            self.item.qty = qty;
            submitNewItem();
            $mdDialog.hide();
        };
    }

    function UpdateQtyCntl($scope, $mdDialog) {
        $scope.updateId = self.updateId;

        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.send=function(qty) {
            self.item.inventory_item_id = self.updateId.id;
            self.item.qty = qty;
            submitNewQty();
            $mdDialog.hide();
        };
    }
}]);

