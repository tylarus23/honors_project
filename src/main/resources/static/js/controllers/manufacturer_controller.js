app.controller("manufacturerController", ['$scope', 'manufacturerService', '$mdDialog', function($scope, manufacturerService, $mdDialog) {
    $scope.headingTitle = "Manufacturers";

    var self = this;

    self.manufacturer = {
        manufacturer_id: null,
        name: ' '
    };

    self.manufacturers=[];

    self.submit = submit;
    self.remove = remove;
    /*
    self.edit = edit;
    self.reset = reset; */

    viewAllManufacturers();

    function viewAllManufacturers() {
        manufacturerService.viewAllManufacturers()
            .then (
                function(data) {
                    self.manufacturers = data;
                },
                function(errResponse) {
                    console.error('Error while fetching Manufacturers');
                }
            );
    }

    function addManufacturer(manufacturer) {
        manufacturerService.addManufacturer(manufacturer)
            .then(
                viewAllManufacturers,
                function(errResponse) {
                    console.error('Error while adding Manufacturers');
                }
            );
    }

    function deleteManufacturer(manufacturer) {
        manufacturerService.deleteManufacturer(manufacturer)
            .then(
                viewAllManufacturers,
                function(errResponse) {
                    console.error('Error while adding Manufacturer');
                    alert("Could not delete manufacturer because it is currently associated with items in inventory");
                }
            )
    }

    function submit() {
        console.log('Saving new Manufacturer', self.manufacturer);
        addManufacturer(self.manufacturer);


        reset();
    }

    function remove(manufacturer) {
        console.log('Deleting Manufacturer', manufacturer);
        deleteManufacturer(manufacturer);
    }

    function reset(){
        self.manufacturer={id:null,name:''};
    }

    // Dialog logic

    $scope.showAddManufacturer = function(ev) {
        $mdDialog.show({
            controller: AddManufacturerCntl,
            templateUrl: '../views/dialogs/add-manufacturer.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: $scope.customFullScreen
        }).then(function(answer) {

        }, function() {

        });
    };


    function AddManufacturerCntl($scope, $mdDialog) {
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.send = function(nm) {
            self.manufacturer.name = nm;
            submit();
            $mdDialog.hide();
        };
    }
}]);
