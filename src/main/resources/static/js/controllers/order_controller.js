app.controller("orderController", ['$scope', 'orderService', 'customerService', 'inventoryService', '$mdDialog', function($scope, orderService, customerService, inventoryService, $mdDialog) {
    $scope.headingTitle="Orders";

    var self = this;

    self.order = {
        order_id: null,
        description: '',
        customer_id: '',
        customer_name: '',
        date_opened: null,
        last_modified_date: null,
        down_payment: null,
        total_price: null,
        status: '',
        notes: '',
        items: null
    };

    self.orderItem = {
        inventory_item_id: null,
        manufacturer_id: '',
        manufacturer_name: '',
        manufacturer_part_num: '',
        distributor_id: '',
        distributor_name: '',
        description: '',
        dist_cost: '',
        sell_price: '',
        min_qty: '',
        max_qty: '',
        qty: '',
        assigned_order: null
    };

    function newItem(inventory_item_id, assigned_order, qty) {
        this.inventory_item_id = inventory_item_id;
        this.assigned_order = assigned_order;
        this.qty = qty;
        this.manufacturer_id = null;
        this.manufacturer_part_num = null;
        this.distributor_id = null;
        this.distributor_name = null;
        this.description = null;
        this.dist_cost = null;
        this.sell_price = null;
        this.min_qty = null;
        this.max_qty = null;
    }

    self.orders = [];
    self.estimates = [];
    self.workOrders = [];
    self.invoices = [];
    self.orderItems = [];
    self.customers = [];
    self.newItems = [];
    self.inventory = [];

    getAllOrders();
    getAllCustomers();
    getAllInventoryItems();
    getOrderToView();

    function getAllOrders() {
        orderService.getAllOrders()
            .then(
                function(data) {
                    self.orders = data;
                    initialize(self.orders);
                },

                function(errResponse) {
                    console.error('Error while fetching Orders');
                }

            );
    }

    function getAllCustomers() {
        customerService.viewAllCustomers()
            .then(
                function(data) {
                    self.customers = data;
                },
                function(errResponse) {
                    console.error('Error while fetching Customers');
                }
            );
    }

    function getAllInventoryItems() {
        inventoryService.viewAllInventory()
            .then(
                function(data) {
                    self.inventory = data;
                },
                function(errResponse) {
                    console.error('Error while fetching inventory');
                }
            );
    }

    // Send order update items to the server.
    function addItems(items) {
        orderService.addOrderItems(items)
            .then(
                function(errResponse) {
                    console.error('Error while adding new items');
                }
            );
    }

    function generateWorkOrder(estimate) {
        estimate.status = 'W';

        orderService.generateWorkOrder(estimate)
            .then(
                getAllOrders,
                function(errResponse) {
                    console.error('Error while generating Work Order');
                }
            );

        window.location="#/orders";
    }

    function generateInvoice(workOrder) {
        orderService.generateInvoice(workOrder)
            .then(
                getAllOrders,
                function(errResponse) {
                    console.error('Error while generating Invoice');
                }
            );

        window.location="#/orders";
    }

    //Determine estimates, work orders, invoices
    function initialize(data) {
        var estimateCount = 0;
        var workOrderCount = 0;
        var invoiceCount = 0;
        var o = data;

        for (var i = 0; i < o.length; i++) {
            if (o.status === 'E') {
                estimateCount++;
            }
            else if (o[i].status === 'W') {
                workOrderCount++;
            }
            else if (o[i].status === 'I') {
                invoiceCount++;
            }
        }

        self.estimates = new Array(estimateCount);
        self.workOrders = new Array(workOrderCount);
        self.invoices = new Array(invoiceCount);

        var a = 0;
        var b = 0;
        var c = 0;

        for (var i = 0; i < o.length; i++) {


            if (o[i].status === 'E') {
                self.estimates[a] = self.orders[i];
                a++;
            }
            else if (o[i].status === 'W') {
                self.workOrders[b] = self.orders[i];
                b++;
            }
            else if (o[i].status === 'I') {
                self.invoices[c] = self.orders[i];
                c++;
            }
        }
    }

    function setEstimateToView(estimate) {
        orderService.setEstimateToView(estimate);

    }

    function getEstimateToView() {
        self.estimateToView = orderService.getEstimateToView();
        $scope.orderData = self.orderToView;

    }

    function setOrderToView(order) {
        orderService.setOrderToView(order);
    }

    function getOrderToView() {
        self.orderToView = orderService.getOrderToView();
        $scope.orderData = self.orderToView;
    }

    function setWorkOrderToView(workOrder) {
        orderService.setWorkOrderToView(workOrder);
    }
    function getWorkOrderToView() {
        self.workOrderToView = orderService.getWorkOrderToView();
        $scope.workOrderData = self.workOrderToView;
    }

    function setInvoiceToView(invoice) {
        orderService.setInvoiceToView(invoice);
    }
    function getInvoiceToView() {
        self.invoiceToView = orderService.getInvoiceToView();
        $scope.invoiceData = self.invoiceToView;
    }

    $scope.generateWorkOrder = function(estimate) {
        generateWorkOrder(estimate);
    };

    $scope.generateInvoice = function(workOrder) {
        generateInvoice(workOrder);
    };

    $scope.openEstimate = function(estimate) {
        setOrderToView(estimate);
        window.location="#/viewEstimate";
    };

    $scope.openWorkOrder = function(workOrder) {
        setOrderToView(workOrder);
        window.location="#/viewWorkOrder";
    };

    $scope.openInvoice = function (invoice) {
        setOrderToView(invoice);
        window.location="#/viewInvoice";
    };

    $scope.refresh = function() {
        reloadOrder(self.orderToView);
    };


    function submit() {
        console.log('Saving new Order', self.order);
        orderService.createNewOrder(self.order)
            .then(
                getAllOrders,
                function(errResponse) {
                    console.error('Error while creating new estimate');
                }
            );
    }

    function submitOrderItems() {
        console.log('Saving Items to Order', self.newItems);
    }

    function reloadOrder(order) {
        getAllOrders();

        for (var i = 0; i < self.orders.length; i++) {
            if (self.orders[i].order_id == order.order_id) {
                self.orderToView = self.orders[i];
            }
        }
    }
    // Dialog logic

    $scope.showAddEstimate = function (ev) {
        $mdDialog.show({
            controller: AddEstimateCtrl,
            templateUrl: '../views/dialogs/add-estimate.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: $scope.customFullScreen
        }).then(function(answer) {

        }, function() {

        });
    };

    function AddEstimateCtrl($scope, $mdDialog) {
        $scope.addCustomers = self.customers;
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.send = function(d, cust) {
            self.order.customer_id = cust;
            self.order.description = d;
            submit();
            $mdDialog.hide();
        };
    }

    $scope.showAddOrderItem = function (ev) {
        $mdDialog.show({
            controller: AddOrderItemsCtrl,
            templateUrl: '../views/dialogs/add-order-items.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: $scope.customFullScreen
        }).then(function(answer) {

        }, function() {

        }).finally(function() {
            reloadOrder(self.orderToView);
        });
    };

    function AddOrderItemsCtrl($scope, $mdDialog) {
        $scope.inventoryItems = self.inventory;

        $scope.hide = function() {
            self.newItems = [];
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            self.newItems = [];
            $mdDialog.cancel();
        };

        $scope.sendItems = function(items) {
            var itemCount = 0;

            for (var i = 0; i < items.length; i++) {
                if (items[i].qty > 0) {
                    itemCount++;
                }
            }

            self.newItems = new Array(itemCount);
            var j = 0; // index of newItems

            for (var i = 0; i < items.length; i++) {
                if (items[i].qty > 0) {

                    var item = new newItem(items[i].inventory_item_id, self.orderToView.order_id, items[i].qty);
                    self.newItems[j] = item;
                    j++;
                }
            }
            addItems(self.newItems);

            $mdDialog.hide();
        };
    }
}]);