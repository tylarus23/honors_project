app.factory("customerService", ['$http', '$q', function($http, $q ) {
    var urlBase = 'http://localhost:8080/customers';

    var factory = {
        viewAllCustomers: viewAllCustomers,
        addCustomer: addCustomer,
        deleteCustomer: deleteCustomer
    };

    return factory;

    function viewAllCustomers() {
        var deferred = $q.defer();

        $http.get(urlBase + '/all')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse){
                    console.error('Error while getting customers');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function addCustomer(customer) {
        var deferred = $q.defer();
        $http.post(urlBase + '/add', customer)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                }, function (errResponse) {
                    console.error('Error while adding Customer');
                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;
    }

    function deleteCustomer(customer) {
        var deferred = $q.defer();

        $http.post(urlBase + '/delete', customer)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                }, function (errResponse) {
                    console.error('Error while deleting Customer');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }
}]);