app.factory("distributorService", ['$http', '$q', function($http, $q ) {
    var urlBase = 'http://localhost:8080/distributor';

    var factory = {
        viewAllDistributors: viewAllDistributors,
        addDistributor: addDistributor,
        deleteDistributor: deleteDistributor
    };

    return factory;

    function viewAllDistributors() {
        var deferred = $q.defer();

        $http.get(urlBase + '/all')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse){
                    console.error('Error while getting distributors');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function addDistributor(distributor) {
        var deferred = $q.defer();
        $http.post(urlBase + '/add', distributor)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                }, function (errResponse) {
                    console.error('Error while adding Distributor');
                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;
    }

    function deleteDistributor(distributor) {
        var deferred = $q.defer();

        $http.post(urlBase + '/delete', distributor)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                }, function (errResponse) {
                    console.error('Error while deleting Distributor');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }
}]);