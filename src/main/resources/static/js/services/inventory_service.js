app.factory("inventoryService", ['$http', '$q', function($http, $q ) {
    var urlBase = 'http://localhost:8080/inventory';

    var factory = {
        viewAllInventory: viewAllInventory,
        getManufacturers: getManufacturers,
        getDistributors: getDistributors,
        addItem: addItem,
        updateQty: updateQty
    };

    return factory;

    function viewAllInventory() {
        var deferred = $q.defer();

        $http.get(urlBase + '/all')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse){
                    console.error('Error while getting inventory');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getManufacturers() {
        var deferred = $q.defer();
        $http.post(urlBase + '/getManufacturers')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while getting manufacturers');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function getDistributors() {
        var deferred = $q.defer();
        $http.post(urlBase + '/getDistributors')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse) {
                    console.error('Error while getting distributors');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function addItem(item) {
        var deferred = $q.defer();
        $http.post(urlBase + '/add', item)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                }, function (errResponse) {
                    console.error('Error while adding Inventory Item');
                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;
    }

    function updateQty(item) {
        var deferred = $q.defer();

        $http.post(urlBase + '/updateQty', item)
            .then(
                function(response) {
                    deferred.resolve(response.data);
                }, function (errResponse) {
                    console.error("Error while updating qty");
                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;
    }
    /*
    function deleteCustomer(customer) {
        var deferred = $q.defer();

        $http.post(urlBase + '/delete', customer)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                }, function (errResponse) {
                    console.error('Error while deleting Customer');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }*/
}]);