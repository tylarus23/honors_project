app.factory("manufacturerService", ['$http', '$q', function($http, $q ) {
    var urlBase = 'http://localhost:8080/manufacturer';

    var factory = {
        viewAllManufacturers: viewAllManufacturers,
        addManufacturer: addManufacturer,
        deleteManufacturer: deleteManufacturer
    };

    return factory;

    function viewAllManufacturers() {
        var deferred = $q.defer();

        $http.get(urlBase + '/all')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function (errResponse){
                    console.error('Error while getting manufacturers');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function addManufacturer(manufacturer) {
        var deferred = $q.defer();
        $http.post(urlBase + '/add', manufacturer)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                }, function (errResponse) {
                    console.error('Error while adding Manufacturer');
                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;
    }

    function deleteManufacturer(manufacturer) {
        var deferred = $q.defer();

        $http.post(urlBase + '/delete', manufacturer)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                }, function (errResponse) {
                    console.error('Error while deleting Manufacturer');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }
}]);