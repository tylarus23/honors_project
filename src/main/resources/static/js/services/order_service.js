app.factory("orderService", ['$http', '$q', '$window', function($http, $q, $window) {
    var urlBase = 'http://localhost:8080/orders'
    var factory = {
        getAllOrders: getAllOrders,
        createNewOrder: createNewOrder,
        addOrderItems: addOrderItems,
        setEstimateToView: setEstimateToView,
        getEstimateToView: getEstimateToView,
        getEstimateItems: getEstimateItems,
        generateWorkOrder: generateWorkOrder,
        setWorkOrderToView: setWorkOrderToView,
        getWorkOrderToView: getWorkOrderToView,
        generateInvoice: generateInvoice,
        setInvoiceToView: setInvoiceToView,
        getInvoiceToView: getInvoiceToView,
        setOrderToView: setOrderToView,
        getOrderToView: getOrderToView
    };

    return factory;

    function getAllOrders() {
        var deferred = $q.defer();

        $http.get(urlBase + '/all')
            .then(
                function(response) {
                    deferred.resolve(response.data);
                    console.log(response.data);
                },

                function(errResponse) {
                    console.error('Error while getting orders');
                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;
    }

    function createNewOrder(order) {
        var deferred = $q.defer();

        $http.post(urlBase + '/add', order)
            .then(
                function(response) {
                    deferred.resolve(response.data);
                    console.log('Creating new order');
                },

                function(errResonse) {
                    console.error('Error while creating new order');
                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;
    }

    function addOrderItems(orderItems) {
        var deferred = $q.defer();

        $http.post(urlBase + '/addOrderItems', orderItems)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                    console.log('Adding Order Items');
                },

                function(errResponse) {
                    console.log('Error Adding Order Items');
                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;
    }

    function generateWorkOrder(estimate) {
        var deferred = $q.defer();

        $http.post(urlBase + '/generateWorkOrder', estimate)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                    console.log('Generating Work Order');
                },

                function(errResponse) {
                    console.log('Error while Generating Work Order');
                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;
    }

    function generateInvoice(workOrder) {
        var deferred = $q.defer();

        $http.post(urlBase + '/generateInvoice', workOrder)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                    console.log('Generating Invoice');
                },

                function(errResponse) {
                    console.log('Error Generating Invoice');
                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;
    }

    /*
    function getOrderItems(order) {
        var deferred = $q.defer();

        $http.post(urlBase + '/getItems', order)
            .then(
                function(response) {
                    deferred.resolve(response.data);
                    console.log(response.data);
                },

                function(errResponse) {
                    console.error('Error while getting Items');
                    deferred.reject(errResponse);
                }
            );

        return deferred.promise;
    }*/

    var estimateToView = null;

    var workOrderToView = null;

    var invoiceToView = null;

    var orderToView = null;

    function setOrderToView(order) {
        orderToView = order;
    }

    function getOrderToView() {
        return orderToView;
    }
    function setEstimateToView(estimate) {
        estimateToView = estimate;
        $window.localStorage.setItem("estimateToView", estimate);
    }

    function getEstimateToView() {
        return estimateToView;
    }

    function getEstimateItems() {
        return estimateToView;
    }

    function setWorkOrderToView(workOrder) {
        workOrderToView = workOrder;
    }

    function getWorkOrderToView() {
        return workOrderToView;
    }

    function setInvoiceToView(invoice) {
        invoiceToView = invoice;
    }

    function getInvoiceToView() {
        return invoiceToView;
    }
}]);