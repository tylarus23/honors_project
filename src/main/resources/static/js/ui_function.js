function hideNav () {
    var nav = document.getElementById("nav-bar");
    var main = document.getElementById("main");

    nav.classList.toggle("nav-bar-hide");
    main.classList.toggle("main_toggled");
}